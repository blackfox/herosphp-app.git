<?php
declare(strict_types=1);

namespace script;

class Installer
{
    public static function install($event)
    {
        $io = $event->getIO();
        static::setRuntimeDir($io);
        static::setPublicDir($io);
        $io->write('success!!!');
    }

    public static function setRuntimeDir($io)
    {
        $dir = __DIR__ . '/../runtime';
        if (! is_dir(__DIR__ . '/../runtime') && ! mkdir($dir, 0777, true) && ! is_dir($dir)) {
            $io->write('Directory "%s" was not created', $dir);
        }
        $io->write('设置 runtime目录成功');
    }

    public static function setPublicDir($io)
    {
        $dir = __DIR__ . '/../public';
        if (! is_dir(__DIR__ . '/../public') && ! mkdir($dir, 0777, true) && ! is_dir($dir)) {
            $io->write('Directory "%s" was not created', $dir);
        }
        $io->write('设置 public目录成功');
    }
}
