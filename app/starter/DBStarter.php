<?php

namespace app\starter;

use herosLdb\LaravelDbStarter;
use herosphp\annotation\Bootstrap;

#[Bootstrap("dbStarter")]
class DBStarter extends LaravelDbStarter
{
    protected static string $pageName = "page";
}

DBStarter::init();